import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';
import UsersComponent from './components/UsersComponent';
import CardComponent from './components/CardComponent';
import TitleComponent from './components/TitleComponent';

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <TitleComponent />
      <UsersComponent />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'grey'
  },
});
