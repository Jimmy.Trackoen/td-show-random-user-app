import React from 'react';
import { FlatList, View } from 'react-native';
import UserService from '../services/UserService';
import CardComponent from './CardComponent';


class UsersComponent extends React.Component {
    constructor(props){
        super(props)
                    
        this.state = {
           usersData: []
        }

        this.showUserAtRegularInterval()
    }

    proxyStatelessComponent( props ){
        return (
            <CardComponent user={props.item} />
        )
    }

    showUserAtRegularInterval(){
        const userService = new UserService()

        setInterval( () => {
           userService.getRandomUser().then( randomUser => {
               let currentUsersData = this.state.usersData;
               currentUsersData.push(randomUser)
    
               this.setState({
                   usersData: currentUsersData
               })
           })

        }, 5000);
    }

    render(){
        return (
            <View>
                <FlatList 
                    data={this.state.usersData}
                    renderItem={this.proxyStatelessComponent.bind(this)}
                    keyExtractor={(item, id) => id.toString()}
                />
            </View>
        )
    }
}

export default UsersComponent
