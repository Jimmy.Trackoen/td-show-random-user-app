import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

class CardComponent extends React.Component {
    constructor(props){
        super(props)
    }

    render(){
        return (
            <View style={styles.card} >
                <Image 
                    source={{ uri: this.props.user.picture }}
                    style={styles.card__image}
                />

                <View>
                    <Text style={styles.text}>{this.props.user.name}</Text>
                    <Text style={styles.text}>{this.props.user.email}</Text>
                    <Text style={styles.text}>{this.props.user.gender}</Text>
                    <Text style={styles.text}>{this.props.user.balance}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    card: {
        margin: 10,
        borderColor: 'white',
        borderWidth: 5,
        padding: 15,
        borderRadius: 5,
        flex: 1,
        flexDirection: 'row'
    },

    card__image: {
        width: 70,
        height: 70,
        borderRadius: 16,
        marginRight: 10
    }
});


export default CardComponent
