import fetch from 'node-fetch';

const RANDOM_USER_API = "https://randomuser.me/api/";

class UserService{

    getRandomUser(){
        const createRandomBalance = () => {
            let dollar = Math.floor(Math.random() * 10000)
            let cents = Math.floor(Math.random() * 50)

            if(dollar > 1000){
                dollar = dollar.toString().split('')
                dollar[1] = ','
                dollar.push( Math.floor(Math.random() * 10) )
                dollar = dollar.join('')
            }

            return `$${ dollar }.${ cents }`
        }

        return new Promise( (resolve,reject) => {
            fetch(RANDOM_USER_API).then( httpResponse => {
                httpResponse.json().then( apiResponse => {
                    let user = apiResponse.results[0]

                    let newUser = {
                        balance: createRandomBalance(),
                        age: user.dob.age,
                        name: `${user.name.first} ${user.name.last}`,
                        gender: user.gender,
                        compagny: "My Compagny",
                        email: user.email,
                        picture: user.picture.thumbnail
                    }

                    resolve(newUser)
                })
            })
        })
    }
}

export default UserService

